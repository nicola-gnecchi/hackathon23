import { StatusBar } from "expo-status-bar";
import axios from "axios";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import React, { useEffect, useState } from "react";
import useEvents from "./hooks/useEvents";
import Paginator from "./Paginator";
import { Picker } from "@react-native-picker/picker";

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

const intervalOverlapsWithMonthYear = (
  fromDate,
  toDate,
  targetYear,
  targetMonth
) => {
  const fromYear = fromDate.getFullYear();
  const fromMonth = fromDate.getMonth();
  const toYear = toDate.getFullYear();
  const toMonth = toDate.getMonth();

  if (fromYear > targetYear || toYear < targetYear) {
    return false; // Interval is completely outside the target year
  }

  if (
    fromYear === targetYear &&
    fromMonth <= targetMonth &&
    toYear === targetYear &&
    toMonth >= targetMonth
  ) {
    return true; // Interval partially or fully overlaps with target month
  }

  return false; // Interval is completely outside the target month
};

const getDate = (item, month, year) => {
  const date = item.EventDate.find((d) => {
    const from = new Date(d.From);
    const to = new Date(d.To);
    if (d.From === "2023-04-02T00:00:00") {
      console.log(
        "DDATE",
        from,
        to,
        month,
        year,
        intervalOverlapsWithMonthYear(from, to, year, month)
      );
    }
    return intervalOverlapsWithMonthYear(from, to, year, month);
  });
  const f = new Date(date?.From || "").toLocaleDateString();
  const t = new Date(date?.To || "").toLocaleDateString();
  return f === t ? f : `${f} - ${t}`;
};

export default function Events() {
  const years = [new Date().getFullYear(), new Date().getFullYear() + 1];
  const [params, setParams] = useState({
    pagenumber: 1,
    month: new Date().getMonth(),
    year: years[0],
    sort: "asc",
  });
  const { status, data } = useEvents(params, { keep: false, debounce: 0 });

  const changeMonth = (m) => {
    setParams((prev) => ({ ...prev, month: m, pagenumber: 1 }));
  };

  const changeYear = (m) => {
    setParams((prev) => ({ ...prev, year: m, pagenumber: 1 }));
  };

  return (
    <View style={styles.container}>
      <View style={{ flexShrink: 0, marginTop: 0, marginBottom: 0 }}>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <Picker
            style={styles.picker}
            selectedValue={params.month}
            onValueChange={(n) => changeMonth(n)}
          >
            {months.map((label, i) => (
              <Picker.Item key={label} label={label} value={i} />
            ))}
          </Picker>
          <Picker
            style={styles.picker}
            selectedValue={params.year}
            onValueChange={(n) => changeYear(n)}
          >
            {years.map((label, i) => (
              <Picker.Item key={label} label={label.toString()} value={label} />
            ))}
          </Picker>
        </View>
      </View>
      <View style={{ flexShrink: 0, marginTop: 0 }}>
        <View
          style={{
            flexDirection: "row",
            columnGap: 8,
            paddingLeft: 8,
            paddingRigt: 8,
            marginLeft: 12,
            marginRight: 12,
            paddingTop: 4,
            paddingBottom: 4,
            backgroundColor: "#109868",
          }}
        >
          <Text
            style={{
              fontWeight: "bold",
              color: "white",
              width: 16,
            }}
          >
            ID
          </Text>
          <Text
            style={{
              fontWeight: "bold",
              color: "white",
            }}
          >
            DATE AND TITLE
          </Text>
        </View>
      </View>
      <ScrollView style={{ flexGrow: 1 }}>
        <View>
          {status === "loading" && (
            <ActivityIndicator
              style={{ marginTop: 40 }}
              size="large"
              animating
            />
          )}
          {status === "success" &&
            data?.data?.Items.map((item, i) => (
              <View
                style={{
                  flexDirection: "row",
                  columnGap: 8,
                  paddingLeft: 20,
                  paddingRight: 20,
                  paddingTop: 4,
                  paddingBottom: 4,
                }}
                key={item.Id}
              >
                <Text style={{ width: 16 }}>
                  {(params.pagenumber - 1) * 10 + i + 1}
                </Text>
                <View style={{ flexDirection: "column" }}>
                  <Text style={{ fontWeight: "bold" }} numberOfLines={1}>
                    {getDate(item, params.month, params.year)}
                  </Text>
                  <Text style={{}} numberOfLines={1}>
                    {item.Detail.it?.Title ||
                      item.Detail.en?.Title ||
                      item.Detail.de?.Title ||
                      "no title"}
                  </Text>
                </View>
              </View>
            ))}
        </View>
      </ScrollView>
      <View style={{ flexShrink: 0, paddingBottom: 16 }}>
        <Paginator
          currentPage={parseInt(params.pagenumber || 0)}
          totalPages={parseInt(data?.data?.TotalPages || 0)}
          onPageChange={(page) => {
            console.log("DDDD", page);
            setParams((prev) => ({ ...prev, pagenumber: page }));
          }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    paddingTop: 0,
  },
  picker: { width: "45%" },
});
