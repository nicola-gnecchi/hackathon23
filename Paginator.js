import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

const Paginator = ({ currentPage, totalPages, onPageChange }) => {
  const handlePageChange = (newPage) => {
    if (onPageChange) {
      onPageChange(newPage);
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.button}
        disabled={currentPage === 1}
        onPress={() => handlePageChange(currentPage - 1)}
      >
        <Text style={styles.buttonText}>Previous</Text>
      </TouchableOpacity>

      <Text style={styles.pageText}>
        Page {currentPage}
        {!!totalPages && ` of ${totalPages}`}
      </Text>

      <TouchableOpacity
        style={styles.button}
        disabled={currentPage === totalPages}
        onPress={() => handlePageChange(currentPage + 1)}
      >
        <Text style={styles.buttonText}>Next</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
  },
  button: {
    padding: 6,
    backgroundColor: "#109868",
    marginHorizontal: 10,
  },
  buttonText: {
    color: "white",
  },
  pageText: {
    fontSize: 16,
  },
});

export default Paginator;
