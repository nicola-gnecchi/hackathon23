import { StatusBar } from "expo-status-bar";
import axios from "axios";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  ScrollView,
  Button,
} from "react-native";
import React, { useEffect, useState } from "react";
import Paginator from "./Paginator";
import useMobility, { MOBILITY_COUNT } from "./hooks/useMobility";

const apis = [
  {
    url: "RWISstation",
    name: "RWI Stations",
  },
  {
    url: "EnvironmentStation",
    name: "Environment Stations",
  },
  {
    url: "LinkStation",
    name: "Link Stations",
  },
  {
    url: "ParkingSensor",
    name: "Parking Sensors",
  },
  {
    url: "ParkingStation",
    name: "Parking Stations",
  },
  {
    url: "MeteoStation",
    name: "Meteo Stations",
  },
  {
    url: "BluetoothStation",
    name: "Bluetooth Stations",
  },
];

export default function Mobility() {
  const [params, setParams] = useState({ pagenumber: 1 });
  const [api, setApi] = useState(apis[0]);
  const { data, status } = useMobility(api.url, params);
  // const status = "loading";
  // const data = {};

  return (
    <View style={styles.container}>
      <View style={{ flexShrink: 0, padding: 12 }}>
        <ScrollView horizontal>
          <View style={{ flexDirection: "row", columnGap: 16 }}>
            {apis.map((a) => (
              <Button
                key={a.name}
                title={a.name}
                onPress={() => {
                  setParams((prev) => ({ ...prev, pagenumber: 1 }));
                  setApi(a);
                }}
                color={a.name === api.name ? "#109868" : "#aaa"}
              />
            ))}
          </View>
        </ScrollView>
      </View>
      <View style={{ flexShrink: 0 }}>
        <View
          style={{
            flexDirection: "row",
            columnGap: 8,
            paddingLeft: 8,
            paddingRigt: 8,
            marginLeft: 12,
            marginRight: 12,
            paddingTop: 4,
            paddingBottom: 4,
            backgroundColor: "#109868",
          }}
        >
          <Text
            style={{
              fontWeight: "bold",
              color: "white",
              width: 26,
            }}
          >
            ID
          </Text>
          <Text
            style={{
              fontWeight: "bold",
              color: "white",
              flexGrow: 1,
            }}
          >
            Name
          </Text>
          <Text
            style={{
              fontWeight: "bold",
              color: "white",
              width: 40,
            }}
          >
            Value
          </Text>
        </View>
      </View>
      <ScrollView style={{ flexGrow: 1 }}>
        <View>
          {status === "loading" && (
            <ActivityIndicator
              style={{ marginTop: 40 }}
              size="large"
              animating
            />
          )}
          {status === "success" &&
            data?.data?.data.map((item, i) => (
              <View
                style={{
                  flexDirection: "row",
                  columnGap: 8,
                  paddingLeft: 20,
                  paddingRight: 20,
                  paddingTop: 4,
                  paddingBottom: 4,
                }}
                key={i}
              >
                <Text style={{ width: 26 }} numberOfLines={1}>
                  {(params.pagenumber - 1) * MOBILITY_COUNT + i + 1}
                </Text>
                <View style={{ flexDirection: "column", flexGrow: 1 }}>
                  <Text style={{}} numberOfLines={1}>
                    {item.tname || "No name"}
                  </Text>
                </View>
                <Text
                  style={{
                    width: 40,
                    textAlign: "right",
                  }}
                  numberOfLines={1}
                >
                  {item.mvalue}
                </Text>
              </View>
            ))}
        </View>
      </ScrollView>
      <View style={{ flexShrink: 0, paddingBottom: 16 }}>
        <Paginator
          currentPage={parseInt(params.pagenumber || 0)}
          totalPages={parseInt(data?.data?.TotalPages || 0)}
          onPageChange={(page) => {
            setParams((prev) => ({ ...prev, pagenumber: page }));
          }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    paddingTop: 0,
  },
  picker: { width: "45%" },
});
