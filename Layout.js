import React, { useRef, useState } from "react";
import {
  Button,
  DrawerLayoutAndroid,
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
} from "react-native";
import { Home } from "./Home";
import Events from "./Events";
import Map from "./Map";
import Accomodations from "./Accomodations";
import BarChartComponent from "./Chart";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { useNavigation } from "@react-navigation/native";
import Mobility from "./Mobility";

const stylesBt = StyleSheet.create({
  button: {
    backgroundColor: "white",
    paddingBottom: 16,
    borderBottomWidth: 1,
    borderColor: "green",
  },
  buttonText: {
    color: "black",
    fontSize: 16,
    fontWeight: "bold",
  },
});

const Stack = createNativeStackNavigator();

const AppDrawer = ({ drawer }) => {
  const navigation = useNavigation();
  return (
    <View style={[styles.navigationContainer]}>
      <Button
        title="Close menu"
        onPress={() => drawer.current.closeDrawer()}
        color="#aaa"
      />

      <TouchableOpacity
        style={stylesBt.button}
        title="Home"
        onPress={() => {
          drawer.current.closeDrawer();
          navigation.navigate("Dashboard");
        }}
      >
        <Text style={stylesBt.buttonText}>Home</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={stylesBt.button}
        title="Events list"
        onPress={() => {
          drawer.current.closeDrawer();
          navigation.navigate("Events list");
        }}
      >
        <Text style={stylesBt.buttonText}>Events list</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={stylesBt.button}
        title="Events map"
        onPress={() => {
          drawer.current.closeDrawer();
          navigation.navigate("Events map");
        }}
      >
        <Text style={stylesBt.buttonText}>Events map</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={stylesBt.button}
        title="Accomodations"
        onPress={() => {
          drawer.current.closeDrawer();
          navigation.navigate("Accomodations");
        }}
      >
        <Text style={stylesBt.buttonText}>Accomodations</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={stylesBt.button}
        title="Events chart"
        onPress={() => {
          drawer.current.closeDrawer();
          navigation.navigate("Events chart");
        }}
      >
        <Text style={stylesBt.buttonText}>Events chart</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={stylesBt.button}
        title="Stations"
        onPress={() => {
          drawer.current.closeDrawer();
          navigation.navigate("Mobility");
        }}
      >
        <Text style={stylesBt.buttonText}>Stations</Text>
      </TouchableOpacity>
    </View>
  );
};

const Layout = ({ navigation }) => {
  const drawer = useRef(null);

  return (
    <DrawerLayoutAndroid
      ref={drawer}
      drawerWidth={300}
      drawerPosition="left"
      renderNavigationView={() => <AppDrawer drawer={drawer} />}
    >
      <View style={[styles.container]}>
        <View style={{ flex: 1, backgroundColor: "#109868" }}>
          <View
            style={{
              flex: 1,
              paddingLeft: 16,
              paddingRight: 16,
              marginTop: 24,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Button
              color="#0a5d41"
              title="☰ menu"
              onPress={() => drawer.current.openDrawer()}
            />
            <Text style={{ fontSize: 20, fontWeight: "bold", color: "white" }}>
              DataPeak
            </Text>
          </View>
        </View>
        <View style={{ flex: 8 }}>
          <Stack.Navigator>
            <Stack.Screen
              name="Dashboard"
              component={Home}
              options={{ title: "Home" }}
            />
            <Stack.Screen
              name="Events list"
              options={{ title: "Events list" }}
              component={Events}
            />
            <Stack.Screen
              options={{ title: "Events map" }}
              name="Events map"
              component={Map}
            />
            <Stack.Screen
              options={{ title: "Accomodations" }}
              name="Accomodations"
              component={Accomodations}
            />
            <Stack.Screen
              options={{ title: "Events chart" }}
              name="Events chart"
              component={BarChartComponent}
            />
            <Stack.Screen
              options={{ title: "Stations" }}
              name="Mobility"
              component={Mobility}
            />
          </Stack.Navigator>
        </View>
      </View>
    </DrawerLayoutAndroid>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  header: {
    backgroundColor: "#aaaaaa",
    flex: 1,
    justifyContent: "space-between",
    padding: 16,
  },
  navigationContainer: {
    padding: 16,
    marginTop: 24,
    rowGap: 20,
  },
  main: {
    flex: 1,
    padding: 16,
    backgroundColor: "red",
  },
});

export default Layout;
