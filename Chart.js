import React, { useState } from "react";
import { View, StyleSheet, Dimensions, Text } from "react-native";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from "react-native-chart-kit";
import useChartEvents from "./hooks/useChartEvents";
import { Picker } from "@react-native-picker/picker";

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

const years = [2022, 2023, 2024];

const BarChartComponent = () => {
  const [month, setMonth] = useState(new Date().getMonth());
  const [year, setYear] = useState(years[0]);
  const { status, data: apiData } = useChartEvents({ month, year });

  if (status === "loading") {
    return null;
  }

  if (status !== "success") {
    return <Text>Error</Text>;
  }

  const data = {
    labels: apiData.data.events_number.map((_, i) =>
      i % 2 === 1 ? "" : i + 1
    ),
    datasets: [
      {
        data: apiData.data.events_number,
      },
    ],
  };

  return (
    <View style={{ padding: 20 }}>
      <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
        <Picker
          style={styles.picker}
          selectedValue={month}
          onValueChange={(n) => setMonth(n)}
        >
          {months.map((label, i) => (
            <Picker.Item key={label} label={label} value={i} />
          ))}
        </Picker>
        <Picker
          style={styles.picker}
          selectedValue={year}
          onValueChange={(n) => setYear(n)}
        >
          {years.map((label, i) => (
            <Picker.Item key={label} label={label.toString()} value={label} />
          ))}
        </Picker>
      </View>
      <LineChart
        data={data}
        width={Dimensions.get("window").width - 40} // from react-native
        height={300}
        yAxisInterval={1} // optional, defaults to 1
        chartConfig={{
          backgroundColor: "#109868",
          backgroundGradientFrom: "#0a5d41",
          backgroundGradientTo: "#1a2d01",
          decimalPlaces: 0, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16,
            paddingLeft: 0,
          },
          propsForDots: {
            r: "4",
            strokeWidth: "1",
            stroke: "#1a2d01",
          },
        }}
        padding="20"
        bezier
        style={{
          marginVertical: 16,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  chart: {
    marginVertical: 8,
    borderRadius: 16,
  },
  picker: { width: "45%" },
});

export default BarChartComponent;
