import React, { useState } from "react";
import { useDebounce } from "@uidotdev/usehooks";
import axios from "axios";
import {
  useQuery,
  useMutation,
  useQueryClient,
  QueryClient,
  QueryClientProvider,
} from "@tanstack/react-query";
import { StyleSheet, View, Text } from "react-native";
import MapView, { Marker } from "react-native-maps";

const calculateRadius = (latitudeDelta) => {
  const earthRadiusMeters = 6371000; // Earth's radius in meters
  const circumference = 2 * Math.PI * earthRadiusMeters;

  // Calculate the radius in meters based on the latitude delta
  const radiusMeters = (latitudeDelta * circumference) / 360;
  return Math.round(radiusMeters); // Return radius in meters
};

const useEvents = ({ latitude, longitude, latitudeDelta }) => {
  const radius = calculateRadius(latitudeDelta);
  const key = JSON.stringify({ latitude, longitude, radius });
  console.log("k", key);
  const debouncedKey = useDebounce(key, 50);
  console.log("dk", debouncedKey);
  const { status, data } = useQuery({
    keepPreviousData: true,
    queryKey: ["event", debouncedKey],
    queryFn: () =>
      axios.get("https://tourism.api.opendatahub.com/v1/Event", {
        params: { latitude, longitude, radius },
      }),
  });
  return { status, data };
};

const MapScreen = () => {
  const [region, setRegion] = useState({
    latitude: 46.6770216,
    longitude: 11.1836174,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  const { data, status } = useEvents(region);
  const markers =
    data?.data?.Items?.map((item) => ({
      id: item?.Id,
      title: item?.Detail?.it?.Title || "",
      latitude: item.Latitude,
      longitude: item.Longitude,
    })) || [];

  const [mapReady, setMapReady] = useState(false);

  const handleMapLayout = () => {
    setMapReady(true);
  };

  const handleRegionChange = (updatedRegion) => {
    setRegion(updatedRegion);
  };

  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        onLayout={handleMapLayout}
        onRegionChangeComplete={handleRegionChange}
        region={region}
        moveOnMarkerPress={false}
      >
        {markers.map((marker) => (
          <Marker
            key={marker.id}
            coordinate={{
              latitude: marker.latitude,
              longitude: marker.longitude,
            }}
            title={marker.title}
          />
        ))}
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    backgroundColor: "red",
  },
  map: {
    flex: 1,
    backgroundColor: "blue",
  },
  text: {
    flex: 1,
    backgroundColor: "blue",
  },
});

export default MapScreen;
