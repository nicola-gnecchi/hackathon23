import requests
import json

url = "https://mobility.api.opendatahub.com/v2/flat%2Cnode/EnvironmentStation/%2A"

querystring = {
        "limit":"200",
        "offset":"0",
        "shownull":"false",
        "distinct":"true"
        }

headers = {
        "accept": "application/json"
        }

response = requests.request("GET", url, headers=headers, params=querystring)

print(json.dumps(response.json(), indent=2))

