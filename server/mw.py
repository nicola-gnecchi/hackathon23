from flask import Flask, request, jsonify
import requests
import json
import math
from datetime import datetime, timedelta
import calendar

N_CLOSEST_ACC = 3
RADIUS_ACC = 1000
USE_ACCOMODATION_CACHED = True
accomodation_cached_file = "accomodations.json"
accomodation_list_cached = []
events_numbers_cached_file = "events_number.json"
events_numbers_dict_cached = {}

app = Flask(__name__)

def load_events_numbers_cache():
    f = open(events_numbers_cached_file)
    tmp = json.load(f)

    for k in tmp:
        events_numbers_dict_cached[k] = tmp[k]

    f.close()

def load_accomodation_list_cache():
    f = open(accomodation_cached_file)
    accomodations = json.load(f)
    f.close()

    for a in accomodations["Items"]:
        a_name = ""
        a_phone = ""
        a_street = ""
        a_website = ""
        longitude = 0
        latitude = 0
        latitude = a["Latitude"]
        longitude = a["Longitude"]
        id = a["Id"]

        if "AccoDetail" in a:
            for k in a["AccoDetail"]:
                if k == "it":
                    a_name = a["AccoDetail"][k]["Name"]
                    a_phone = a["AccoDetail"][k]["Phone"]
                    a_street = a["AccoDetail"][k]["Street"]
                    a_website = a["AccoDetail"][k]["Website"]

        if len(a_name) == 0:
            a_name = "No titolo"

        accomodation_list_cached.append(
                {
                    "Id": id,
                    "Name": a_name,
                    "Latitude": latitude,
                    "Longitude": longitude,
                    "Street": a_street,
                    "Phone": a_phone,
                    "Website": a_website,
                })

def calculate_distance(lat1, lon1, lat2, lon2):
    # approximate radius of earth in km
    R = 6371.0

    lat1_rad = math.radians(lat1)
    lon1_rad = math.radians(lon1)
    lat2_rad = math.radians(lat2)
    lon2_rad = math.radians(lon2)

    dlon = lon2_rad - lon1_rad
    dlat = lat2_rad - lat1_rad

    a = math.sin(dlat / 2)**2 + math.cos(lat1_rad) * math.cos(lat2_rad) * math.sin(dlon / 2)**2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    return R * c

def retrieve_generic_json_open_data_hub(query):
    response = requests.get(query)
    return response.json()

def retreive_events(longitude, latitude, radius):
    # url = f"https://tourism.opendatahub.com/v1/Event?pagenumber=1&pagesize=64&latitude={latitude}&longitude={longitude}&radius={radius}&removenullvalues=true"
    url = f"https://tourism.opendatahub.com/v1/Event?pagenumber=1&pagesize=10&latitude={latitude}&longitude={longitude}&radius={radius}&removenullvalues=true"

    return retrieve_generic_json_open_data_hub(url)

def retrieve_accomodations(longitude, latitude, radius, n):

    ret = []
    tmp = []

    if not USE_ACCOMODATION_CACHED:
        # build the URL for the Accommodation endpoint
        url = f"https://tourism.opendatahub.com/v1/Accommodation?pagenumber=1&typefilter=73&roominfo=1-18%2C18&bokfilter=hgv&msssource=sinfo&availabilitychecklanguage=en&detail=0&latitude={latitude}&longitude={longitude}&radius={radius}&removenullvalues=false"

        accomodations = retrieve_generic_json_open_data_hub(url)

        for a in accomodations["Items"]:
            a_name = ""
            a_phone = ""
            a_street = ""
            a_website = ""
            longitude = 0
            latitude = 0
            latitude = a["Latitude"]
            longitude = a["Longitude"]
            id = a["Id"]

            if "AccoDetail" in a:
                for k in a["AccoDetail"]:
                    if k == "it":
                        a_name = a["AccoDetail"][k]["Name"]
                        a_phone = a["AccoDetail"][k]["Phone"]
                        a_street = a["AccoDetail"][k]["Street"]
                        a_website = a["AccoDetail"][k]["Website"]

            if len(a_name) == 0:
                a_name = "No titolo"

            tmp.append(
                    {
                        "Id": id,
                        "Name": a_name,
                        "Latitude": latitude,
                        "Longitude": longitude,
                        "Street": a_street,
                        "Phone": a_phone,
                        "Website": a_website,
                    })
    else:
        tmp = accomodation_list_cached
    
    if len(tmp) <= n:
        ret = tmp
    else:
        sorted_tmp = sorted(tmp, key=lambda item: calculate_distance(latitude,
                                                                     longitude,
                                                                     item['Latitude'],
                                                                     item['Longitude']))
        ret = sorted_tmp[0:n]

    return ret

def retrieve_accomodations_for_events(events_json, radius):

    ret_json = {"Events": []}

    if events_json.get("Items"):
        e_title = ""
        latitude = 0
        longitude = 0
        datebegin = "1970-01-01T00:00:00"
        dateend = "1970-01-01T00:00:00"
        for e in events_json["Items"]:
            if e["Detail"]:
                for k in e["Detail"]:
                    if k == "it":
                        if "Title" in e["Detail"][k]:
                            e_title = e["Detail"][k]["Title"]

            if len(e_title) == 0:
                e_title = "No titolo"

            latitude = e["Latitude"]
            id = e["Id"]
            longitude = e["Longitude"]
            datebegin = e["DateBegin"]
            dateend = e["DateEnd"]

            acc = retrieve_accomodations(longitude, latitude, RADIUS_ACC, N_CLOSEST_ACC)

            ret_json["Events"].append(
                    {
                        "Title": e_title,
                        "Id": id,
                        "Latitude": latitude,
                        "Longitude": longitude,
                        "DateBegin": datebegin,
                        "DateEnd": dateend,
                        "Accomodations": acc
                    })

    return json.dumps(ret_json, indent=2)

@app.route('/eventsnumbers', methods=['GET'])
def events_numbers():
    year = int(request.args.get("year"))
    month = int(request.args.get("month"))
    _, num_days = calendar.monthrange(year, month)
    start_date = datetime.strptime(f"{year}-{month}-01", "%Y-%m-%d")
    end_date = datetime.strptime(f"{year}-{month}-{num_days}", "%Y-%m-%d")

    date = start_date

    js = {"events_number": []}

    while date <= end_date:
        date_str = date.strftime("%Y-%m-%d")
        if date_str in events_numbers_dict_cached:
            js["events_number"].append(events_numbers_dict_cached[date_str])
        else:
            break

        date += timedelta(days=1)

    return js

@app.route('/eventsaccommodations', methods=['GET'])
def events_accommodations():
    longitude = request.args.get('longitude')
    latitude = request.args.get('latitude')
    radius = request.args.get('radius')

    # Retrieve all events
    events = retreive_events(longitude, latitude, radius)

    ev_acc = retrieve_accomodations_for_events(events, 1000)

    return ev_acc


if __name__ == '__main__':
    load_accomodation_list_cache()
    load_events_numbers_cache()
    app.run(host='0.0.0.0', debug=True)
