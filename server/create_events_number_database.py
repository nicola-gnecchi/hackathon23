import requests
import sys
import json
from datetime import datetime, timedelta

def fetch_events(start_date, end_date):
    ret_json = {}
    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.strptime(end_date, "%Y-%m-%d")

    date = start_date

    while date <= end_date:
        # Convert date back to string
        date_str = date.strftime("%Y-%m-%d")
        
        url = f"https://tourism.opendatahub.com/v1/Event?pagenumber=1&begindate={date_str}&enddate={date_str}&removenullvalues=false"
        
        response = requests.get(url)

        j = response.json()
        print("{}: {}".format(date_str, j["TotalResults"]))
        ret_json[date_str] = j["TotalResults"]
        
        # Move on to the next date
        date += timedelta(days=1)

    return json.dumps(ret_json, indent=2)

if __name__ == "__main__":
    # fetch_events("2023-08-01", "2023-08-10")

    if len(sys.argv) < 3:
        print(f"{sys.argv[0]} 2023-08-01 2023-08-10")
        sys.exit(1)

    j = fetch_events(sys.argv[1], sys.argv[2])

    print(j)

    with open('events_number.json', 'w') as outfile:
        outfile.write(j)

