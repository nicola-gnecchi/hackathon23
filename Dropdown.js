import { Picker } from "@react-native-picker/picker";
import React, { useState } from "react";
import { View, Text, StyleSheet } from "react-native";

const SelectMonthComponent = ({ onChange, value }) => {
  const getCurrentMonth = (monthIndex) => {
    const months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    return months[monthIndex % 12];
  };

  const generateMonths = () => {
    const currentMonth = new Date().getMonth() + 1;
    const months = [];
    for (let i = currentMonth; i <= currentMonth + 12; i++) {
      const currentYear = new Date().getFullYear() + Math.floor(i / 12);
      months.push(
        <Picker.Item
          key={i}
          label={getCurrentMonth(i) + " " + currentYear}
          value={{ month: i % 12, year: currentYear }}
        />
      );
    }
    return months;
  };

  return (
    <View style={styles.container}>
      <Picker
        style={styles.picker}
        selectedValue={value}
        onValueChange={onChange}
      >
        {generateMonths()}
      </Picker>
      <Text style={styles.selectedMonthText}>
        {getCurrentMonth(value.month)} {value.year}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  picker: {
    width: 200,
    marginTop: 10,
    marginBottom: 20,
  },
  selectedMonthText: {
    marginTop: 10,
    fontSize: 16,
  },
});

export default SelectMonthComponent;
