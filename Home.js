import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";

export const Home = () => {
  return (
    <View
      style={[
        styles.container,
        {
          // Try setting `flexDirection` to `"row"`.
          flexDirection: "column",
        },
      ]}
    >
      <View style={{ marginBottom: 20 }}>
        <Text>Welcome to DataPeak!</Text>
      </View>
      <View>
        <Image
          style={{
            width: "100%",
            height: undefined,
            aspectRatio: 1,
          }}
          source={require("./assets/logo.jpg")}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
});
