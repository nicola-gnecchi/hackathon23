import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { backendUrl } from "../config";

const useChartEvents = ({ month, year }, { keep = true } = {}) => {
  const params = {
    year,
    month: month + 1,
  };
  const key = JSON.stringify(params);
  const { status, data } = useQuery({
    keepPreviousData: keep,
    queryKey: ["chart", key],
    queryFn: () =>
      axios.get(backendUrl + "/eventsnumbers", {
        params,
      }),
  });
  console.log(status, data);
  return { status, data };
};

export default useChartEvents;
