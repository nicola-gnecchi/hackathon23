import { useDebounce } from "@uidotdev/usehooks";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";

const calculateRadius = (latitudeDelta) => {
  const earthRadiusMeters = 6371000; // Earth's radius in meters
  const circumference = 2 * Math.PI * earthRadiusMeters;

  // Calculate the radius in meters based on the latitude delta
  const radiusMeters = (latitudeDelta * circumference) / 360;
  return Math.round(radiusMeters); // Return radius in meters
};

const getMonthRange = (month, year) => {
  const startDate = new Date(year, month, 1);
  const endDate = new Date(year, month + 1, 0); // Set day to 0 of next month to get last day of current month

  const format = (date) => {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const day = String(date.getDate()).padStart(2, "0");
    return `${year}-${month}-${day}`;
  };

  return {
    begindate: format(startDate),
    enddate: format(endDate),
  };
};

const useEvents = (
  { latitude, longitude, latitudeDelta, pagenumber, month, year, sort },
  { debounce = 2000, keep = true } = {}
) => {
  const dates = month && year ? getMonthRange(month, year) : {};
  const radius = latitudeDelta && calculateRadius(latitudeDelta);
  const params = {
    latitude,
    longitude,
    radius,
    pagenumber,
    sort,
    ...dates,
  };
  console.log("PARS", params);
  const key = JSON.stringify(params);
  const debouncedKey = useDebounce(key, debounce);
  const { status, data } = useQuery({
    keepPreviousData: keep,
    queryKey: ["event", debouncedKey],
    queryFn: () =>
      axios.get("https://tourism.api.opendatahub.com/v1/Event", {
        params,
      }),
  });
  return { status, data };
};

export default useEvents;
