import { useQuery } from "@tanstack/react-query";
import axios from "axios";

export const MOBILITY_COUNT = 20;

const useMobility = (api, { pagenumber }) => {
  const count = MOBILITY_COUNT;
  const params = {
    offset: count * (pagenumber - 1),
    limit: count,
    distinct: true,
    timezone: "UTC",
    shownull: false,
  };
  const url = `https://mobility.api.opendatahub.com/v2/flat%2Cnode/${api}/%2A/latest`;
  console.log(url);
  const { status, data } = useQuery({
    queryKey: [url, pagenumber],
    queryFn: () =>
      axios.get(url, {
        params,
      }),
  });
  return { status, data };
};

export default useMobility;
